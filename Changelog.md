# Changelog

## Release 1.1.1
* Removed unused package

## Release 1.1.0 (latest)
* Added possibility to use UnCached functionality

## Release 1.0.0 Initial Release
* Initial publication