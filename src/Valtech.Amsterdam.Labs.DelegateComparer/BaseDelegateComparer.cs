﻿using System;
using System.Collections.Generic;
using System.Data;

// ReSharper disable VirtualMemberCallInConstructor

namespace Valtech.Amsterdam.Labs.DelegateComparer
{
    /// <summary>
    /// A comparer simply using a <see cref="Func{TResult}"/>
    /// I can't believe they haven't thought of this themselves
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public abstract class BaseDelegateComparer<TModel> : IComparer<TModel>
    {
        /// <summary>
        /// Actual comparing Func
        /// </summary>
        protected virtual Func<TModel, TModel, int> CompareFunc { get; set; }

        /// <inheritdoc cref="IComparer{TModel}"/>
        /// <exception cref="T:System.Data.EvaluateException">
        /// Something went wrong evaluating the expression </exception>
        public int Compare(TModel first, TModel second)
        {
            // Check Nulls
            if (CompareFunc == null)
                throw new EvaluateException("Something went wrong evaluating the expression");

            // Try to evaluate
            try
            {
                return CompareFunc(first, second);
            }
            catch (Exception ex)
            {
                throw new EvaluateException("Something went wrong evaluating the expression", ex);
            }
        }
    }
}
