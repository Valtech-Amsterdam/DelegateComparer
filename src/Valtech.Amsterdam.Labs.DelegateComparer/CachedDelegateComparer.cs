﻿using System;
using System.Linq.Expressions;
using System.Runtime.Caching;

namespace Valtech.Amsterdam.Labs.DelegateComparer
{
    /// <inheritdoc cref="BaseDelegateComparer{TModel}"/>
    public class CachedDelegateComparer<TModel> : BaseDelegateComparer<TModel>
    {
        private readonly Expression<Func<TModel, TModel, int>> _compareExpression;
        private readonly MemoryCache _defaultMemoryCache;
        private readonly string _expressionCacheKey;

        /// <inheritdoc />
        protected override Func<TModel, TModel, int> CompareFunc
        {
            get
            {
                if (_defaultMemoryCache.Contains(_expressionCacheKey))
                    return (Func<TModel, TModel, int>)_defaultMemoryCache.Get(_expressionCacheKey);

                var compiledExpression = _compareExpression.Compile();
                CompareFunc = compiledExpression;
                return compiledExpression;
            }

            set
            {
                if (_defaultMemoryCache.Contains(_expressionCacheKey)) return;

                var compiledExpression = _compareExpression.Compile();
                _defaultMemoryCache.Add(_expressionCacheKey, compiledExpression,
                    new CacheItemPolicy { SlidingExpiration = ObjectCache.NoSlidingExpiration });
            }
        }

        /// <inheritdoc cref="DelegateComparer{TModel}"/>
        /// <param name="compareExpression">Expression used to compare enumerables</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="compareExpression" /> is null. </exception>
        public CachedDelegateComparer(Expression<Func<TModel, TModel, int>> compareExpression)
        {
            if (compareExpression == null) throw new ArgumentNullException(nameof(compareExpression));
            
            // Don't want to cache output, only the body of the expression so we only compile once
            _defaultMemoryCache = MemoryCache.Default;
            _expressionCacheKey = $"{compareExpression}-{typeof(TModel).FullName}";
            _compareExpression = compareExpression;
        }
    }
}
