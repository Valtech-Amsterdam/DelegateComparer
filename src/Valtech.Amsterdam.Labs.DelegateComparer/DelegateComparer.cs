﻿using System;
using System.Data;
using System.Linq.Expressions;

namespace Valtech.Amsterdam.Labs.DelegateComparer
{
    /// <inheritdoc cref="BaseDelegateComparer{TModel}"/>
    public class DelegateComparer<TModel> : BaseDelegateComparer<TModel>
    {
        /// <inheritdoc cref="BaseDelegateComparer{TModel}"/>
        /// <param name="compareExpression">Expression used to compare enumerables</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="compareExpression" /> is null. </exception>
        public DelegateComparer(Expression<Func<TModel, TModel, int>> compareExpression)
        {
            // ReSharper disable once VirtualMemberCallInConstructor
            CompareFunc = compareExpression?.Compile() 
                ?? throw new NoNullAllowedException(nameof(compareExpression));
        }
    }
}
