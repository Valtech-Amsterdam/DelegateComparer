﻿using System.Diagnostics.CodeAnalysis;

namespace Valtech.Amsterdam.Labs.DelegateComparer.Tests.Models
{
    [ExcludeFromCodeCoverage]
    public class AlphabetSorted
    {
        public char SortIndex { get; set; }
    }
}
