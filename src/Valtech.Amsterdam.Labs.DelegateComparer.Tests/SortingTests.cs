﻿using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Caching;
using NUnit.Framework;
using Valtech.Amsterdam.Labs.DelegateComparer.Tests.Models;

namespace Valtech.Amsterdam.Labs.DelegateComparer.Tests
{
    [TestFixture] public class SortingTests
    {
        private const string TinyAlphabet = "ABCD";

        [Test] public void SortByAlphabet()
        {
            // Assign
            var a = new AlphabetSorted { SortIndex = 'A' };
            var b = new AlphabetSorted { SortIndex = 'B' };
            var c = new AlphabetSorted { SortIndex = 'C' };
            var d = new AlphabetSorted { SortIndex = 'D' };
            var unsorted = new [] {b, c, d, a};
            var expected = new [] {a, b, c, d};

            // Act
            var sorted = unsorted.OrderBy(x => x, 
                new DelegateComparer<AlphabetSorted>((first, second) => AlphabetOrderingMethod(first, second)))
                .ToArray();

            // Assert
            Assert.That(expected, Is.EqualTo(sorted));
        }

        [Test] public void SortByAlphabetCached()
        {
            // Assign
            var a = new AlphabetSorted { SortIndex = 'A' };
            var b = new AlphabetSorted { SortIndex = 'B' };
            var c = new AlphabetSorted { SortIndex = 'C' };
            var d = new AlphabetSorted { SortIndex = 'D' };
            var unsorted = new[] { b, c, d, a };
            var unsorted2 = new[] { c, d, b, a };
            var expected = new[] { a, b, c, d };

            // Act
            var sorted = unsorted.OrderBy(x => x,
                new CachedDelegateComparer<AlphabetSorted>((first, second) => AlphabetOrderingMethod(first, second)))
                .ToArray();
            var sorted2 = unsorted2.OrderBy(x => x,
                new CachedDelegateComparer<AlphabetSorted>((first, second) => AlphabetOrderingMethod(first, second)))
                .ToArray();

            // Assert
            Assert.That(MemoryCache.Default.Contains(
                $"(first, second) => AlphabetOrderingMethod(first, second)-{typeof(AlphabetSorted).FullName}"), Is.True);
            Assert.That(expected, Is.EqualTo(sorted));
            Assert.That(expected, Is.EqualTo(sorted2));
        }

        [Test, Ignore("Todo fix the error on build server: System.MissingMethodException : Method not found: 'System.Array.Empty'")]
        public void SortByAlphabet_ExpressionFails_ThrowsError()
        {
            // Assign
            var a = new AlphabetSorted { SortIndex = 'A' };
            var unsorted = new[] { a };

            // Act
            var sorted = unsorted.OrderBy(x => x,
                new DelegateComparer<AlphabetSorted>((first, second) => FailingMethod()));

            // Assert
            Assert.That(() => sorted.GetEnumerator().MoveNext(),
                Throws.Exception.TypeOf<EvaluateException>());
        }

        [ExcludeFromCodeCoverage] private static int FailingMethod() 
            => throw new Exception("This method always fails");

        [ExcludeFromCodeCoverage]
        private static int AlphabetOrderingMethod(AlphabetSorted first, AlphabetSorted second)
        {
            Assert.NotNull(first); Assert.NotNull(second);

            var sortIndexFirst = TinyAlphabet.IndexOf(first.SortIndex);
            var sortIndexSecond = TinyAlphabet.IndexOf(second.SortIndex);

            if (sortIndexFirst > sortIndexSecond) return 1;
            if (sortIndexFirst < sortIndexSecond) return -1;
            return 0;
        }
    }
}
