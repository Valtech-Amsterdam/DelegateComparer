[//]: # (Header)

<div align="center">
  <a href="https://gitlab.com/Valtech-Amsterdam/DelegateComparer">
    <img height="250" width="250" src="https://gitlab.com/Valtech-Amsterdam/DelegateComparer/raw/master/resource/DelegateComparer.png" alt="DelegateComparer">
  </a>
</div>

[![Downloads][downloads-image]][nuget-url] 
[![Build Status][build-image]][build-url] [![Coveralls Status][coveralls-image]][coveralls-url]

[//]: # (Documentation)

## What is DelegateComparer  
By default the .Net framework requires you to create an implemenation of ```IComparer<TModel>``` if you want to use the ```IOrderedEnumerable.SortBy(comparer)``` functionality.  
In some cases you might just want a method from the class you are performing the operation or maybe even a single line (which could be a lambda for instance).  
For the above cases the DelegateComparer exists, you create a delegateComparer that expects the sorting Func instead of an implementation that has the sorting func.  
  
_[Valtech.Amsterdam.Labs.Linq.SorterExtensions](https://gitlab.com/Valtech-Amsterdam/Linq.SorterExtensions) provides an implementation of this class inside of the Linq framework._

## Why do I need this?
Honestly I hope you never have to implement custom sorting logic inside of your linq chain ever. However, sometimes you have to.  
This library is usefull if you have custom sorting logic depending on parents and you need to sort on a name property after that.  
Or if you have to custom sort an object you don't have controll over, for example a library or a framework like a CMS.

## Installing the library
Simply check out the [NuGet Package][nuget-url] using:  
```Batchfile
Install-Package Valtech.Amsterdam.Labs.DelegateComparer
```
  
## Using the library  
Simply call one of these:  
```csharp
	{enumerable}.OrderBy(x => x, 
        new DelegateComparer<{Type}>((first, second) => {expression}));
```  
```csharp
	{enumerable}.OrderBy(x => x, 
        new CachedDelegateComparer<{Type}>((first, second) => {expression}));
```  
The Cached delegate comparer stores the compiled expression in the cache using the expression string and the full TypeName.  
See a full usage example here: [src/Valtech.Amsterdam.Labs.DelegateComparer.Tests/SortingTests.cs](https://gitlab.com/Valtech-Amsterdam/DelegateComparer/blob/master/src/Valtech.Amsterdam.Labs.DelegateComparer.Tests/SortingTests.cs)  
  
## [Contributing][contributing-url]  
[contributing-url]: https://gitlab.com/Valtech-Amsterdam/DelegateComparer/blob/master/Contributing.md
See the [Contribution guide][contributing-url] for help about contributing to this project.
  
## [Changelog][changelog-url]  
[changelog-url]: https://gitlab.com/Valtech-Amsterdam/DelegateComparer/blob/master/Changelog.md
See the [Changelog][changelog-url] to see the change history.

[//]: # (Labels)

[downloads-image]: https://img.shields.io/nuget/dt/Valtech.Amsterdam.Labs.DelegateComparer.svg
[nuget-url]: https://www.nuget.org/packages/Valtech.Amsterdam.Labs.DelegateComparer
[build-url]: https://gitlab.com/Valtech-Amsterdam/DelegateComparer/pipelines
[build-image]: https://gitlab.com/Valtech-Amsterdam/DelegateComparer/badges/master/build.svg
[coveralls-url]: https://gitlab.com/Valtech-Amsterdam/DelegateComparer/commits/master
[coveralls-image]: https://gitlab.com/Valtech-Amsterdam/DelegateComparer/badges/master/coverage.svg?job=test