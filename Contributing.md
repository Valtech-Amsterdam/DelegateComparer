# Contribution guide  
Contributions are much appreciated.

## Fixing a bug
If you'd like to fix a bug from the issues list please assign it to yourself and create a branch with 'feature/{issueNr}'.  
If you want to fix a but that is not in the issue list, please creat an issue first and proceed with the above.
  
When your change is done please create a merge request to the release branch the ticket is assigned to,   
If the change is valid you are requested to merge the branch yourself and close the issue once the merge is complete.  
  
### Branching model
The branching model is the [Feature branch][atlassian-branching-models-url] model.  
To read more about it see: [The atlassian branching models documentation][atlassian-branching-models-url].  
  
## Deployments
The deployments are completely automated based on the Git-Tags, as soon as a tag is made the pipeline will publish the package to npmjs.org.  
The responsibility for releasing is scoped to project admins.

[//]: # (Labels)

[atlassian-branching-models-url]: https://www.atlassian.com/git/tutorials/comparing-workflows#feature-branch-workflow